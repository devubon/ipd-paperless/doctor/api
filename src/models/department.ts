import { UUID } from "crypto";
import { Knex } from "knex";

export class DepartmentModel {
  constructor() { }

  /**
   * 
   * @param db 
   * @param departmentID 
   * @returns 
   */
  getByID(db: Knex, departmentID: UUID) {
    return db('h_department')
    .where('id', departmentID)
    .select(
      'id', 'name', 'code'
    )
    .first();
  }

  /**
   * 
   * @param db 
   * @param departmentIDs 
   * @returns 
   */
  getByIDs(db: Knex, departmentIDs: UUID[]) {
    return db('h_department')
    .whereIn('id', departmentIDs)
    .select(
      'id', 'name', 'code'
    );
  }

}