import { UUID } from "crypto";
import { Knex } from "knex";

export class InsuranceModel {
  constructor() { }

  getByID(db: Knex, insuranceID: UUID) {
    return db('h_insurance')
      .where('id', insuranceID)
      .select(
        'id', 'name', 'code'
      )
      .first();
  }
  /**
   * 
   * @param db 
   * @param insuranceIDs 
   * @returns 
   */
  getByIDs(db: Knex, insuranceIDs: UUID[]) {
    return db('h_insurance')
      .whereIn('id', insuranceIDs)
      .select(
        'id', 'name', 'code'
      )
  }

}