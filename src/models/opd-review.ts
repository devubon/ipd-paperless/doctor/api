import { Knex } from 'knex';
import { UUID } from 'crypto';

export class OpdReviewModel {
  getByAdmitID(db: Knex, admitID: UUID) {
    return db('opd_review')
      .where('admit_id', admitID)
      .select(
        'id', 'an', 'vn', 'chief_complaint', 'present_illness', 'past_history', 'physical_exam',
        'body_temperature', 'body_weight', 'body_height', 'waist', 'pulse_rate', 'respiratory_rate',
        'systolic_blood_pressure', 'diatolic_blood_pressure',
        'oxygen_sat', 'eye_score', 'movement_score', 'verbal_score'
      )
      .first()
  }
}
