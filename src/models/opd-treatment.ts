import { Knex } from 'knex';
import { UUID } from 'crypto';

export class OpdReviewModel {
  getOpdReviewByAdmitID(db: Knex, admitID: UUID) {
    return db('opd_treatment as o')
      .leftJoin('h_item_type as h', 'o.item_type_id','h.id')
      .where('o.admit_id', admitID)
      .select(
        'o.id as opd_treatment_id', 'o.admit_id',
        'o.an', 'o.quantity', 'o.price', 'o.item_type_id', 'o.item_code', 'o.item_name', 
        'o.treatment_date', 'o.treatment_time', 'o.treatment_by', 'o.item_describe',
        'h.name as item_type_name',
        )
  }
}