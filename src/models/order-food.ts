import { Knex } from 'knex';
import { UUID } from 'crypto';

export class OrderFoodModel {
  create(db: Knex, data: any) {
    return db('order_food')
      .returning('id')
      .insert(data);
  }

  update(db: Knex, data: any, orderFoodID: UUID) {
    return db('order_food')
      .where('order_food_id', orderFoodID)
      .update(data);
  }

  upsert(db: Knex, data: any) {
    return db('order_food')
      .insert(data)
      .onConflict('doctor_order_id')
      .merge()
      .returning('*');
  }

  getByDoctorOrderID(db: Knex, doctorOrderID: UUID) {
    return db('order_food')
      .where('doctor_order_id', doctorOrderID)
      .andWhere('is_active', true)
      .select(
        'id', 'doctor_order_id', 'food_id',
        'start_date', 'start_time', 'end_date', 'end_time'
      );
  }

  // getAll(db: Knex, search: any, limit: number, offset: number) {
  getAll(db: Knex) {
    let sql = db('order_food')
      .select(
        'id', 'doctor_order_id', 'food_id',
        'start_date', 'start_time', 'end_date', 'end_time'
      )
    return sql

    // if (search) {
    //   let query = `%${search}%`
    //   sql.where(builder => {
    //     builder.whereRaw('doctor_order_id = ?', [query])
    //       .orWhereRaw('food_id = ?', [query])
    //       .orWhereRaw('start_date = ?', [query])
    //       .orWhereRaw('start_time = ?', [query])
    //       .orWhereRaw('end_date = ?', [query])
    //       .orWhereRaw('end_time = ?', [query])
    //   })
    // }

    // return sql.limit(limit).offset(offset);
  }

  // getTotal(db: Knex, search: any) {
  getTotal(db: Knex) {
    let sql = db('order_food')
    return sql
      .count({ total: '*' }).first();

    // if (search) {
    //   let query = `%${search}%`
    //   sql.where(builder => {
    //     builder.whereRaw('doctor_order_id = ?', [query])
    //       .orWhereRaw('food_id = ?', [query])
    //       .orWhereRaw('start_date = ?', [query])
    //       .orWhereRaw('start_time = ?', [query])
    //       .orWhereRaw('end_date = ?', [query])
    //       .orWhereRaw('end_time = ?', [query])
    //   })
    // }

    // return sql
    //   .count({ total: '*' }).first();
  }
}
