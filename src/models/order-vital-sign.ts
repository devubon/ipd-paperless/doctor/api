import { Knex } from 'knex';
import { UUID } from 'crypto';
export class OrderVitalSignModel {
  getByDoctorOrderID(db: Knex, doctorOrderID: UUID) {
    return db('order_vital_sign')
      .where('doctor_order_id', doctorOrderID)
      .andWhere('is_active', true)
      .select(
        'id', 'admit_id', 'is_vital_sign',
        'start_date', 'start_time', 'end_date', 'end_time',
      )
  }

  create(db: Knex, data: any) {
    return db('order_vital_sign')
    .returning('id')
    .insert(data)
  }

  update(db: Knex, data: any, orderVitalSingID: UUID) {
    return db('order_vital_sing')
    .where('id', orderVitalSingID)
    .update(data)
  }

  delete(db: Knex, orderVitalSingID: UUID) {
    return db('order_vital_sign')
    .returning('id')
    .where('id', orderVitalSingID)
    .delete()
  }

  upsert(db: Knex, data: any) {
    return db('order_vital_sign')
    .insert(data)
    .onConflict('id')
    .merge()
    .returning('*')
  }
}
