import { Knex } from 'knex';
import { UUID } from 'crypto';

export class OrderModel {

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: any) {
    return db('orders')
    .returning('id')
    .insert(data)
    .returning('*');
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param orderID 
   * @returns 
   */
  update(db: Knex, data: any, orderID: UUID) {
    return db('orders')
    .where('id', orderID)
    .update(data)
    .returning('*');
  }

  /**
   * 
   * @param db 
   * @param orderID 
   * @returns 
   */
  delete(db: Knex, orderID: UUID) {
    return db('orders')
    .where('id', orderID)
    .delete();
  }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  upsert(db: Knex, data: any) {
    return db('orders')
      .insert(data)
      .onConflict('id')
      .merge()
      .returning('*');
  }

  /**
   * 
   * @param db 
   * @param doctorOrderID 
   * @returns 
   */
  getByDoctorOrderID(db: Knex, doctorOrderID: UUID) {
    return db('orders as or')
      .leftJoin('h_items as hi', 'or.item_id', 'hi.id')
      .where('or.doctor_order_id', doctorOrderID)
      .select(
        'or.id', 'or.doctor_order_id',
        'or.order_type_id', 'or.item_type_id', 'or.item_id','hi.code', 'or.item_name',
        'or.medicine_usage_code', 'or.medicine_usage_extra',
        'or.quantity',
        'or.is_confirm', 'or.confirm_by', 'or.confirm_date', 'or.confirm_time',
        'or.is_process', 'or.process_by'
      )
  }
}