import { Knex } from 'knex';
import { UUID } from 'crypto';

export class PatientAllergyModel {
  getByAdmitID(db: Knex, admitID: UUID) {
    return db('patient_allergy')
      .where('admit_id', admitID)
      .select(
        'id', 'admit_id', 'hn', 'drug_name',
        'allergy_level', 'allergy_date', 'symptom' 
      )
  }
}