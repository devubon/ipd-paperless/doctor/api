import { Knex } from 'knex';
import { UUID } from 'crypto';

export class PatientHistoryModel {

  delete(db: Knex, id: UUID) {
    return db('patient_history')
      .where('id', id)
      .delete()
      .returning('id');
  }

  update(db: Knex, data: any, id: UUID) {
    return db('patient_history')
      .where('id', id)
      .update(data)
      .returning('id');
  }

  create(db: Knex, data: any) {
    return db('patient_history')
      .insert(data)
      .returning('id');
  }

  upsert(db: Knex, data: any) {
    return db('patient_history')
      .insert(data)
      .onConflict('admit_id')
      .merge()
      .returning('*');
  }

  updateByAdmitID(db: Knex, data: object, admitID: UUID) {
    return db('patient_history')
      .where('admit_id', admitID)
      .update(data)
      .returning('id');
  }

  getByAdmitID(db: Knex, admitID: UUID) {
    return db('patient_history')
      .where('admit_id', admitID)
      .andWhere('is_active', true)
      // .select(
      //   'admit_id','is_chronic','chronic_describe','is_allergy',
      //   'allergy_describe','is_operation', 'operation_describe',
      //   'is_family_disease', 'family_disease_describe',
      //   'last_menstrual_period', 'is_child_vaccine_complete',
      //   'is_child_development_normal','is_smoke','is_drink_alchohol',
      //   'smoke_describe','drink_alchohol_describe','reference_by',
      //   'is_birth_control','birth_control_describe'
      // )
  }
}
