import { Knex } from 'knex';
import { UUID } from 'crypto';

export class PatientPregnantModel {
  
  delete(db: Knex, id: UUID) {
    return db('patient_pregnant')
    .where('id', id)
    .delete();
  }
  
  update(db: Knex, data: any, id: UUID) {
    return db('patient_pregnant')
    .where('id', id)
    .update(data);
  }

  create(db: Knex, data: any) {
    return db('patient_pregnant')
    .insert(data);
  }

  upsert(db: Knex, data: any) {
    return db('patient_pregnant')
    .insert(data)
    .onConflict('admit_id')
    .merge()
    .returning('*');
  }

  updateByAdmitID(db: Knex, data: object, admitID: UUID) {
    return db('patient_pregnant')
    .where('admit_id', admitID)
    .update(data)
    .returning('id');
  }
  getByAdmitID(db: Knex, admitID: UUID) {
    return db('patient_pregnant')
      .where('admit_id', admitID)
      .andWhere('is_active', true)
      // .select(
      //   'admit_id','gravida','para','abortion','live_birth','note'
      // )
  }
}