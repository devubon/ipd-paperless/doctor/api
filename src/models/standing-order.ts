import { Knex } from 'knex';
import { UUID } from 'crypto';

export class StandingOrderModel {

  constructor() { }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: object) {
    return db('standing_order')
      .returning('id')
      .insert(data);
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param standingOrderID 
   * @returns 
   */
  update(db: Knex, data: object, standingOrderID: UUID) {
    return db('standing_order')
      .returning('id')
      .where('id', standingOrderID)
      .update(data)
  }

  /**
   * 
   * @param db 
   * @param standingOrderID 
   * @returns 
   */
  delete(db: Knex, standingOrderID: UUID) {
    return db('standing_order')
      .returning('id')
      .where('id', standingOrderID)
      .delete();
  }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  upsert(db: Knex, data: any) {
    return db('updating_progress_note')
      .insert(data)
      .onConflict('id')
      .merge();
  }

  /**
   * 
   * @param db 
   * @param standingOrderID 
   * @returns 
   */
  getByID(db: Knex, standingOrderID: UUID) {
    return db('standing_order')
      .where('id', standingOrderID)
      .andWhere('is_active', true)
      .select(
        'id', 'order_type_id', 'user_id', 'group_disease_id', 'item_type_id',
        'item_id', 'item_name', 'quantity',
        'medicine_usage', 'medicine_usage_extra',
        'department_id'
      );
  }

  /**
   * 
   * @param db 
   * @param groupDiseaseID 
   * @returns 
   */
  getByGroupDiseaseID(db: Knex, groupDiseaseID: number) {
    return db('standing_order')
      .where('group_disease_id', groupDiseaseID)
      .andWhere('is_active', true)
      .select(
        'id', 'order_type_id', 'user_id', 'group_disease_id', 'item_type_id',
        'item_id', 'item_name', 'quantity',
        'medicine_usage', 'medicine_usage_extra',
        'department_id'
      );
  }

  getByDepartmentID(db: Knex, departmentID: UUID) {
    return db('standing_order')
      .where('department_id', departmentID)
      .andWhere('is_active', true)
      .select(
        'id', 'order_type_id', 'user_id', 'group_disease_id', 'item_type_id',
        'item_id', 'item_name', 'quantity',
        'medicine_usage', 'medicine_usage_extra',
        'department_id'
      );
  }
  getList(db: Knex) {
    return db('standing_order');
  }

  getGroupDiseaseByStandingOrder(db: Knex) {
    return db('l_group_disease').select(db.raw('l_group_disease.*')).innerJoin('standing_order','standing_order.group_disease_id','l_group_disease.id').groupBy(db.raw('l_group_disease.id'))
  }

}
