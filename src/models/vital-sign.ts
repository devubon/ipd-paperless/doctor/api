import { Knex } from 'knex';
import { UUID } from 'crypto';

export class VitalSignModel {
  constructor () { }

  create(db: Knex, data: any) {
    return db('vital_sign')
    .insert(data);
  }

  update(db: Knex, data: any, vitalSignID: UUID) {
    return db('vital_sign')
    .where('id', vitalSignID)
    .update(data);
  }

  delete(db: Knex, vitalSignID: UUID) {
    return db('vital_sign')
    .where('id', vitalSignID)
    .delete();
  }

  upsert(db: Knex, data: any) {
    return db('vital_sign')
    .insert(data)
    .onConflict('id')
    .merge();
  }

  getByAdmitID(db: Knex, admitID: UUID) {
    return db('vital_sign')
    .where('admit_id', admitID)
    .andWhere('is_active', true)
    .select(
      'id', 'admit_id', 'vital_sign_date','vital_sign_time',
      'body_temperature', 'pulse_rate', 'respiratory_rate',
      'systolic_blood_pressure', 'diatolic_blood_pressure',
      'intake_oral_fluid', 'intake_penterate', 'intake_medicine',
      'outtake_urine', 'outtake_emesis', 'outtake_drainage', 'outtake_aspiration', 'outtake_lochia',
      'stools', 'urine', 'pain_score', 'oxygen_sat', 'body_weight'
    );
  }

}