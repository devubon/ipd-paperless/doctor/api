import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { UUID } from 'crypto';
import { DischargeSummaryModel } from '../../models/discharge-summary';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const dischargeSummaryModel = new DischargeSummaryModel()

  // post discharge summary (create discharge summary)
  // /doctor/discharge-summary
  fastify.post('/', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body;
    const data: any = { ...body }

    try {
      const dischargeSummary: any = await dischargeSummaryModel.create(db, data);
      const lastID: any = dischargeSummary[0]?.id;

      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          id: lastID
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // put discharge summary (update discharge summary)
  // /doctor/discharge-summary
  fastify.put('/:discharge_summary_id', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const dischargeSummaryID: UUID = params.discharge_summary_id
    const body: any = request.body;
    const data: any = { ...body }

    try {
      const dischargeSummary: any = await dischargeSummaryModel.update(db, data, dischargeSummaryID);
      const lastID: any = dischargeSummary[0].id

      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          id: lastID
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // get discharge summary by id 
  // /doctor/discharge-summary/:discharge_summary_id
  fastify.get('/:discharge_summary_id', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const dischargeSummaryID: UUID = params.discharge_summary_id
    var data: any = {}
    try {
      const dischargeSummary: any = await dischargeSummaryModel.getByID(db, dischargeSummaryID);
      data = { ...dischargeSummary }
      
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  done();
}



