import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./admit'), { prefix: '/doctor/admit' });
  fastify.register(require('./ward'), { prefix: '/doctor/ward' });
  fastify.register(require('./admission-note'), { prefix: '/doctor/admission-note' });
  fastify.register(require('./doctor-order'), { prefix: '/doctor/doctor-order' });
  fastify.register(require('./order'), { prefix: '/doctor/order' });
  fastify.register(require('./order-food'), { prefix: '/doctor/order-food' });
  fastify.register(require('./progress-note'), { prefix: '/doctor/progress-note' });
  fastify.register(require('./standing-progress-note'), { prefix: '/doctor/standing-progress-note' });
  fastify.register(require('./standing-order'), { prefix: '/doctor/standing-order' });
  fastify.register(require('./nurse-note'), { prefix: '/doctor/nurse-note' });
  fastify.register(require('./vital-sign'), { prefix: '/doctor/vital-sign' });
  fastify.register(require('./medicine-usage'), { prefix: '/doctor/medicine-usage' });
  fastify.register(require('./discharge-summary'), { prefix: '/doctor/discharge-summary' });

  done();

} 
