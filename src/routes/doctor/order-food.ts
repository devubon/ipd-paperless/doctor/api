import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { OrderFoodModel } from '../../models/order-food';
import { UUID } from 'crypto';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const orderFoodModel = new OrderFoodModel();

  // get order food
  // /doctor/order-food
  fastify.get('/', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const _query: any = request.query;
    const { limit, offset, search } = _query;
    const _limit = limit || 20;_
    const _offset = offset || 0;

    try {
      const data: any = await orderFoodModel.getAll(db);
      // const data: any = await orderFoodModel.getAll(db, search, _limit, _offset);
      // const total: any = await orderFoodModel.getTotal(db, search);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data,
          // total: Number(total.total)
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

    // get order food
  // /doctor/order-food
  fastify.get('/:doctor_order_id', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const doctorOrderID: UUID = params.doctor_order_id;

    try {
      const data: any = await orderFoodModel.getByDoctorOrderID(db, doctorOrderID) 
      // const data: any = await orderFoodModel.getAll(db, search, _limit, _offset);
      // const total: any = await orderFoodModel.getTotal(db, search);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data,
          // total: Number(total.total)
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  done();
}