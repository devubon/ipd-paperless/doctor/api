import S from 'fluent-json-schema'

//Create By RENFIX
const schema = S.object()
  .prop('nurse_note_date', S.string().format('date').required())
  .prop('nurse_note_time', S.string().format('time').required())
  //.prop('problem_list', S.string().maxLength(250).required()) // ตัดออกเพราะ เป็น Json
 // .prop('activity', S.string().maxLength(250).required()) // ตัดออกเพราะ เป็น Json
 // .prop('evaluate', S.string().maxLength(250).required()) // ตัดออกเพราะ เป็น Json
  //.prop('create_date', S.string().format('date').required())
  .prop('create_by', S.string().format('uuid'))
 // .prop('modify_date', S.string().format('time').required())
  .prop('modify_by', S.string().format('uuid'))


export default {
  body: schema
}