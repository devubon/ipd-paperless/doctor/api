import S from 'fluent-json-schema'

const schema = S.object()
  .prop('insurance_id', S.string().format('uuid').required())
  .prop('department_id', S.string().format('uuid').required())
  .prop('pre_diag', S.string().maxLength(250).required())
  .prop('ward_id', S.string().format('uuid').required())
  .prop('bed_id', S.string().format('uuid'))
  .prop('doctor_id', S.string().format('uuid'))

export default {
  body: schema
}